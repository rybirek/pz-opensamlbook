# pz-opensamlbook

Przykładowa implementacja komunikacji z PZ

AuthnRequest -> Artifact

ArtifactResolve -> ArtifactResponse

LogoutRequest -> LogoutResponse

AccessFilter przechwytuje wywołania do zasobu chronionego i inicjuje komunikację z PZ.

ConsumerServlet przyjmuje zwrócony artefakt (SAMLart) i rozwiązuje go przy pomocy idpArtifactResolutionService.

LogoutFilter przychwytuje żądanie wylogowania i wywołuje wylogowanie w PZ

Konfiguracja na sztywno w klasie Constants

```java


public class Constants {

    // SP
    // Nazwa SAML (Issuer)
    public static final String SP_ENTITY_ID = "TestSP";
    // Adres zwrotny dla usĹugi SSO
    public static final String ASSERTION_CONSUMER_SERVICE = "http://localhost:8080/pz-opensamlbook/saml/SSO";

	...
    public static final String KEY_STORE_PASSWORD = "password";
    public static final String KEY_STORE_ENTRY_PASSWORD = "password";
    public static final String KEY_STORE_PATH = "/SPKeystore.jks";
    public static final String KEY_ENTRY_ID = "SPKey";

}
```

i mapowanie w ConsumerServlet

```java

@WebServlet("/saml/SSO")
public class ConsumerServlet extends HttpServlet {
...
```


Implementacja bazuje na:

https://wiki.shibboleth.net/confluence/display/OpenSAML/Home

https://github.com/rasmusson/webprofile-ref-project - źródła do książki https://gumroad.com/l/a-guide-to-opensaml

Przykładowe requesty

https://bitbucket.org/snippets/rybirek/ky7Aeb


TODO:

Brak analizy zwrotnego komunikatu wylogowania.