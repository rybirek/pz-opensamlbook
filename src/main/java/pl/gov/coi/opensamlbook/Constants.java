package pl.gov.coi.opensamlbook;

public class Constants {

    // SP
    // Nazwa SAML (Issuer)
    public static final String SP_ENTITY_ID = "TestSP";
    // Adres zwrotny dla uslugi SSO
    public static final String ASSERTION_CONSUMER_SERVICE = "http://localhost:8080/pz-opensamlbook/saml/SSO";
    public static final String AUTHENTICATED_SESSION_ATTRIBUTE = "authenticated";
    public static final String USER_SESSION_ATTRIBUTE = "user";
    public static final String GOTO_URL_SESSION_ATTRIBUTE = "gotoURL";


    // IDP
    public static final String SSO_SERVICE = "https://int.pz.gov.pl/dt/SingleSignOnService";
    public static final String ARTIFACT_RESOLUTION_SERVICE = "https://int.pz.gov.pl/dt-services/idpArtifactResolutionService";
    public static final String SLO_SERVICE = "https://int.pz.gov.pl/dt/SingleLogoutService";


    public static final String KEY_STORE_PASSWORD = "password";
    public static final String KEY_STORE_ENTRY_PASSWORD = "password";
    public static final String KEY_STORE_PATH = "/SPKeystore.jks";
    public static final String KEY_ENTRY_ID = "SPKey";


}
