package pl.gov.coi.opensamlbook;


import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.joda.time.DateTime;
import org.opensaml.Configuration;
import org.opensaml.DefaultBootstrap;
import org.opensaml.common.SAMLObject;
import org.opensaml.common.binding.BasicSAMLMessageContext;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.saml2.binding.encoding.HTTPPostEncoder;
import org.opensaml.saml2.core.AuthnRequest;
import org.opensaml.saml2.core.Issuer;
import org.opensaml.saml2.core.LogoutRequest;
import org.opensaml.saml2.core.NameID;
import org.opensaml.saml2.metadata.Endpoint;
import org.opensaml.saml2.metadata.SingleSignOnService;
import org.opensaml.saml2.metadata.SingleLogoutService;
import org.opensaml.ws.message.encoder.MessageEncodingException;
import org.opensaml.ws.transport.http.HttpServletResponseAdapter;
import org.opensaml.xml.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.Provider;
import java.security.Security;

@WebFilter("/logout")
public class LogoutFilter implements Filter {

    private static Logger logger = LoggerFactory.getLogger(LogoutFilter.class);
    private VelocityEngine velocityEngine;

    public void init(FilterConfig filterConfig) throws ServletException {
        Configuration.validateJCEProviders();
        Configuration.validateNonSunJAXP();

        for (Provider jceProvider : Security.getProviders()) {
            logger.info(jceProvider.getInfo());
        }

        try {
            logger.info("Bootstrapping");
            DefaultBootstrap.bootstrap();
        } catch (ConfigurationException e) {
            throw new RuntimeException("Bootstrapping failed");
        }

        try {
            velocityEngine = new VelocityEngine();
            velocityEngine.setProperty(RuntimeConstants.ENCODING_DEFAULT, "UTF-8");
            velocityEngine.setProperty(RuntimeConstants.OUTPUT_ENCODING, "UTF-8");
            velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
            velocityEngine.setProperty("classpath.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
            velocityEngine.init();
        } catch (Exception e) {
            logger.debug("Error initializing velicoity engige", e);
            throw new RuntimeException("Error configuring velocity", e);
        }
    }
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest)request;
        HttpServletResponse httpServletResponse = (HttpServletResponse)response;
        if (httpServletRequest.getSession().getAttribute(Constants.AUTHENTICATED_SESSION_ATTRIBUTE) == null) {
            chain.doFilter(request, response);
        } else {
            redirectUserForLogout(httpServletRequest, httpServletResponse);
        }
    }

    private void redirectUserForLogout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        HttpSession session=httpServletRequest.getSession();
        String userLogin = session.getAttribute(Constants.USER_SESSION_ATTRIBUTE).toString();
        LogoutRequest logoutRequest = buildLogoutRequest(userLogin);
        redirectUserWithRequest(httpServletResponse, logoutRequest);
    }


    private void redirectUserWithRequest( HttpServletResponse httpServletResponse, LogoutRequest logoutRequest) {
        HttpServletResponseAdapter responseAdapter = new HttpServletResponseAdapter(httpServletResponse, true);

        BasicSAMLMessageContext<SAMLObject, LogoutRequest, SAMLObject> context = new BasicSAMLMessageContext<SAMLObject, LogoutRequest, SAMLObject>();
        context.setPeerEntityEndpoint(getIPDEndpoint());
        context.setOutboundSAMLMessage(logoutRequest);
        context.setOutboundMessageTransport(responseAdapter);
        context.setOutboundSAMLMessageSigningCredential(Credentials.getCredential());

        HTTPPostEncoder encoder = new HTTPPostEncoder(velocityEngine, "/templates/saml2-post-binding.vm");
        logger.info("LogoutRequest: ");
        OpenSAMLUtils.logSAMLObject(logoutRequest);

        logger.info("Redirecting to IDP");
        try {
            encoder.encode(context);
        } catch (MessageEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private LogoutRequest buildLogoutRequest(String userLogin){
        LogoutRequest logoutRequest = OpenSAMLUtils.buildSAMLObject(LogoutRequest.class);
        Issuer issuer = OpenSAMLUtils.buildSAMLObject(Issuer.class);
        issuer.setValue(Constants.SP_ENTITY_ID);
        logoutRequest.setIssuer(issuer);
        logoutRequest.setID(OpenSAMLUtils.generateSecureRandomId());
        logoutRequest.setIssueInstant(new DateTime());
        logoutRequest.setDestination(getIPDSLODestination());
        NameID nameID = OpenSAMLUtils.buildSAMLObject(NameID.class);
        nameID.setFormat(NameID.UNSPECIFIED);
        nameID.setValue(userLogin);
        logoutRequest.setNameID(nameID);

        return logoutRequest;
    }

    private Endpoint getIPDEndpoint() {
        SingleLogoutService endpoint = OpenSAMLUtils.buildSAMLObject(SingleLogoutService.class);
        endpoint.setBinding(SAMLConstants.SAML2_REDIRECT_BINDING_URI);
        endpoint.setLocation(getIPDSLODestination());

        return endpoint;
    }

    private String getIPDSLODestination() {
        return Constants.SLO_SERVICE;
    }


    public void destroy() {

    }
}
